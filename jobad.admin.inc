<?php

function jobad_admin_page() {
  $form = array();

  $form['jobad_maxdisp'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of job ads in a job listing page'),
    '#default_value' => variable_get('jobad_maxdisp', 10),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t("The maximum number of job ads in a job listing page."),
    '#required' => TRUE,
  );
  
  $form['jobad_privatekey'] = array(
    '#type' => 'textfield',
    '#title' => t('Gownjob.com public key of your site'),
    '#default_value' => variable_get('jobad_privatekey', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => t("Gownjob.com private and public key of your site can be retieved from <a href='http://www.gownjob.com/'>gownjob.com</a> after registration."),
    '#required' => FALSE,
  );
  
  $form['jobad_publickey'] = array(
    '#type' => 'textfield',
    '#title' => t('Gownjob.com private key of your site'),
    '#default_value' => variable_get('jobad_publickey', ''),
    '#size' => 60,
    '#maxlength' => 128,
    '#description' => t("Gownjob.com private and public key of your site can be retieved from <a href='http://www.gownjob.com/'>gownjob.com</a> after registration."),
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

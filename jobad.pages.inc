<?php

function jobad_page() {
  drupal_set_title(t('Listing of Job Category'));
  $output = "<div class='jobad'>";
  $list = jobad_list_category();
  while ($category = db_fetch_object($list)) {
    //l($category->name,'taxonomy/term/'.$category->tid)
    $output .= t("<div class='jobad_catagory_!cid'>!category </div>", array('!cid' => $category->tid, '!category' => l($category->name,'jobad/category/'.$category->tid)));
  }
  $output .= "</div>";
  return $output;
}

<?php

function theme_jobad_company($node) {
  $list = _company_bynid($node->company);
  $company = db_fetch_object($list);
  if ($node->company > 0)
    $output = t("<div>by !company</div>", array('!company' => l($company->title,'node/'.$node->company)));  
  return $output;
}

function theme_jobad_description($node) {
  $output = t("<div class='jobad_description'><p><strong>Job Description</strong></p><p>!content</p></div>", array('!content' => $node->description));
  return $output;
}

function theme_jobad_requirement($node) {
  $output = t("<div class='jobad_requirement'><p><strong>Job Requirement</strong></p><p>!content</p></div>", array('!content' => $node->body));
  return $output;
}

function theme_jobad_application($node) {
  $output = '<div class="jobad_application">';
  $output .= t('<strong>Application Method</strong><p>!content</p>', array('!content' => $node->application));
  $output .= '</div>';
  return $output;
}

function theme_jobad_duedate($node) {
  $output = '<div class="jobad_duedate">';
  $output .= t('<strong>Application Due Date</strong><p>!content</p>', array('!content' => $node->duedate));
  $output .= '</div>';
  return $output;
}

function theme_company_infofield($node) {
  $output = '<div class="company">';
  if ($node->body != '') {
    $output .= t('<strong>Company Information</strong><br>');
  }
  if ($node->phone != '') {
    $output .= t('Phone: %phone<br>', array('%phone' => check_plain($node->phone)));
  }
  if ($node->fax != '') {
    $output .= t('Fax: %fax<br>', array('%fax' => check_plain($node->fax)));
  }
  if ($node->email != '') {
    $output .= t('Email Address: %email<br>', array('%email' => check_plain($node->email)));
  }
  if ($node->website != '') {
    $output .= t('Website URL: <a href="http://%url">%website</a><br>', array('%url' => $node->website,'%website' => check_plain($node->website)));
    drupal_set_title(t("<a href='!url'>!name</a>", array('!url' => check_plain($node->website), '!name' => $node->title)));
  }
  if ($node->person != '') {
    $output .= t('Contact Person: %person<br>', array('%person' => check_plain($node->person)));
  }        
  $output .= '</div>';
  return $output;
}

